import std/[algorithm, parseopt, sequtils, strutils, sets, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val


if puzzle == "one" or puzzle == "both":
  var res = 0
  for line in lines(input):
    res += line.split(" | ")[1].split(" ").countIt(it.len in {2, 3, 4, 7})
  echo "puzzle one: ", res

if puzzle == "two" or puzzle == "both":
  var res = 0
  for line in lines(input):
    let l = line.split(" | ")
    let q = l[0].split(" ").sortedByIt(it.len).mapIt(it.toHashSet)
    var x: Table[HashSet[char], char]
    x[q[0]] = '1'
    x[q[1]] = '7'
    x[q[2]] = '4'
    x[q[9]] = '8'
    for i in 3..5:
      if len(q[i] * q[0]) == 2:
        x[q[i]] = '3'
      elif len(q[i] + q[2]) == 7:
        x[q[i]] = '2'
      else:
        x[q[i]] = '5'
    for i in 6..8:
      if len(q[i] * q[2]) == 4:
        x[q[i]] = '9'
      elif len(q[i] * q[0]) == 2:
        x[q[i]] = '0'
      else:
        x[q[i]] = '6'
    var c = ""
    for r in l[1].split(" "):
      c.add(x[r.toHashSet])
    res += c.parseInt
  echo "puzzle two: ", res
