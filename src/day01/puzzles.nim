import std/[parseopt, sequtils, strutils, sugar]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let
  nums = readFile(input).strip().splitLines().map(parseInt)

func incInSeq(seq: seq[int]): int =
  for i in 0 .. (seq.len - 2):
    if seq[i + 1] > seq[i]:
      inc result

if puzzle == "one" or puzzle == "both":
  echo "puzzle one: ", incInSeq(nums)

if puzzle == "two" or puzzle == "both":
  let winSum = collect(newSeq):
    for i in 0 .. (nums.len - 3):
      nums[i] + nums[i + 1] + nums[i + 2]
  echo "puzzle two: ", incInSeq(winSum)
