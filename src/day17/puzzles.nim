import std/[parseopt, sequtils, strutils]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let
  nums = input.readFile.strip.split(",").mapIt(
    it.split("=")[1].split("..").map(parseInt)
  )
  (minTargetX, maxTargetX) = (nums[0][0], nums[0][1])
  (minTargetY, maxTargetY) = (nums[1][0], nums[1][1])

var largestY, validStartVs = 0
for startVx in 0..maxTargetX:
  for startVy in minTargetY..(-minTargetY - 1):
    var (highest, px, py, vx, vy) = (0, 0, 0, startVx, startVy)
    while true:
      px.inc(vx)
      py.inc(vy)
      if (px in minTargetX..maxTargetX) and (py in minTargetY..maxTargetY):
        largestY = max(highest, largestY)
        validStartVs.inc
        break
      if py < minTargetY:
        break
      if vx > 0:
        vx.dec
      vy.dec
      if vy == 0:
        highest = py

if puzzle == "one" or puzzle == "both":
  echo "puzzle one: ", largestY

if puzzle == "two" or puzzle == "both":
  echo "puzzle two: ", validStartVs
