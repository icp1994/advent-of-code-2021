import std/[parseopt, sequtils, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

var hexMap: Table[char, string]
for digit in 0..15:
  hexMap[digit.toHex(1)[0]] = digit.toBin(4)

let bits = input.readFile.strip.foldl(a & hexMap[b], "")
var expr: seq[string] = @[]
var versum = 0

proc decode(currLen: int, currPaks: int, lenPaks: int, nbPaks: int): int =
  versum.inc(bits[currLen .. currLen + 2].parseBinInt)
  let typeId = bits[currLen + 3 .. currLen + 5]
  var ncl: int

  if typeId == "100":
    ncl = currLen + 6
    var packet = ""
    while true:
      packet.add(bits[ncl + 1 .. ncl + 4])
      if bits[ncl] == '0':
        break
      ncl.inc(5)
    expr.add(packet)
    ncl.inc(5)
  else:
    expr.add("(")
    expr.add(typeId)
    if bits[currLen + 6] == '0':
      ncl = decode(currLen + 22, currPaks, currLen + 22 + bits[currLen + 7 ..
          currLen + 21].parseBinInt, -1)
    else:
      ncl = decode(currLen + 18, currPaks, -1, currPaks + bits[currLen + 7 ..
          currLen + 17].parseBinInt)
    expr.add(")")

  let ncp = currPaks + 1
  if (ncl == lenPaks) or (ncp == nbPaks):
    return ncl
  return decode(ncl, ncp, lenPaks, nbPaks)

discard decode(0, 0, -1, 1)
if puzzle == "one" or puzzle == "both":
  echo "puzzle one: ", versum

if puzzle == "two" or puzzle == "both":
  var reducer: seq[string]
  while expr.len != 0:
    let elem = expr.pop
    if elem != "(":
      reducer.add(elem)
    else:
      let op = reducer.pop()
      var args: seq[int]
      while true:
        let arg = reducer.pop()
        if arg == ")":
          break
        args.add(arg.parseBinInt)

      let output = case op
        of "000":
          args.foldl(a + b, 0)
        of "001":
          args.foldl(a * b, 1)
        of "010":
          args.foldl(min(a, b), high(int))
        of "011":
          args.foldl(max(a, b), 0)
        of "101":
          if args[0] > args[1]: 1 else: 0
        of "110":
          if args[0] < args[1]: 1 else: 0
        of "111":
          if args[0] == args[1]: 1 else: 0
        else:
          raise
      reducer.add(output.toBin(64))

  assert reducer.len == 1
  echo "puzzle two: ", reducer[0].parseBinInt
