import std/[algorithm, parseopt, sequtils, strutils, enumerate, tables, sets]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

var caveDepths: Table[(int, int), int]
for i, line in enumerate(lines(input)):
  for j, c in line:
    caveDepths[(i, j)] = ($c).parseInt

func nbrs(p: (int, int)): array[4, (int, int)] =
  let (x, y) = p
  return [(x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)]

var lowPoints: HashSet[(int, int)]
for pos, depth in caveDepths.pairs:
  if pos.nbrs.allIt(depth < caveDepths.getOrDefault(it, 9)):
    lowPoints.incl(pos)

if puzzle == "one" or puzzle == "both":
  echo lowPoints.foldl(a + caveDepths[b] + 1, 0)

proc genBasin(seed: (int, int), output: var HashSet[(int, int)]) =
  let depth = caveDepths.getOrDefault(seed, 9)
  if depth != 9:
    output.incl(seed)
    for pos in nbrs(seed):
      if depth < caveDepths.getOrDefault(pos, 9):
        genBasin(pos, output)

if puzzle == "two" or puzzle == "both":
  var basins: seq[HashSet[(int, int)]]
  var unexplored = lowPoints
  while unexplored.len != 0:
    let seed = unexplored.pop
    var newBasin: HashSet[(int, int)]
    genBasin(seed, newBasin)
    basins.add(newBasin)
    unexplored = unexplored - newBasin

  echo basins.sortedByIt(it.len)[^3 .. ^1].foldl(a * b.len, 1)
