import std/[parseopt, strutils, enumerate, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

type
  Position = tuple[x: int, y: int]
  Octopus = tuple[energy: int, flashed: bool]

var octoGrid: Table[Position, Octopus]
for row, line in enumerate(lines(input)):
  for col, val in line:
    octoGrid[(row, col)] = (($val).parseInt, false)

func nbrs(pos: Position): array[8, (int, int)] =
  let (x, y) = pos
  result = [
    (x - 1, y - 1), (x - 1, y), (x - 1, y + 1), (x, y + 1),
    (x + 1, y + 1), (x + 1, y), (x + 1, y - 1), (x, y - 1)
  ]

proc process(pos: Position, octo: var Octopus) =
  if not octo.flashed:
    octo.energy.inc
    if octo.energy > 9:
      octo.energy = 0
      octo.flashed = true
      for nbr in pos.nbrs:
        if octoGrid.hasKey(nbr):
          process(nbr, octoGrid[nbr])

const targetSteps = 100
var steps, flashes, syncSteps = 0
var syncedOnce = false
while (steps < targetSteps) or (not syncedOnce):
  for pos, octo in octoGrid.mpairs:
    process(pos, octo)
  var flashesInStep = 0
  for octo in octoGrid.mvalues:
    if octo.flashed:
      flashesInStep.inc
      octo.flashed = false

  steps.inc
  if steps < targetSteps:
    flashes.inc(flashesInStep)
  elif steps == targetSteps:
    flashes.inc(flashesInStep)
    if puzzle == "one":
      echo "puzzle one: ", flashes
      break

  if (flashesInStep == octoGrid.len) and (not syncedOnce):
    syncSteps = steps
    syncedOnce = true
    if puzzle == "two":
      echo "puzzle two: ", syncSteps
      break

if puzzle == "both":
  echo "puzzle one: ", flashes
  echo "puzzle two: ", syncSteps
