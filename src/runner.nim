import std/[os, parseopt, strformat, strutils, times]

let
  bindir = getAppDir()
  srcdir = joinPath(bindir.parentDir(), "src")

var
  d = (now().utc - 5.hours).format("MMM dd").split()
  month = d[0]
  day = if month != "Dec": "25" else: d[1]
  puzzle = "both"
  input: string

for kind, key, val in getopt():
  case kind
  of cmdLongOption, cmdShortOption:
    case key
    of "day", "d":
      try:
        type Day = range[1..25]
        let d: Day = val.parseInt
        day = fmt"{d:02}"
      except ValueError, RangeDefect:
        echo fmt"option `{key}` must be an integer in the range `1 .. 25`"
        echo fmt"using a relevant day ({day.parseInt}) as default", "\n"
    of "puzzle", "p":
      case val
      of "one", "two", "both":
        puzzle = val
      else:
        echo fmt"option `{key}` must be either `one`, `two`, or `both`"
        echo "using `both` as default\n"
  of cmdArgument:
    input = key
  of cmdEnd:
    assert(false)

if input == "":
  input = fmt"{srcdir}/day{day}/input"

let
  srcpath = fmt"{srcdir}/day{day}/puzzles.nim"
  cmdHead = fmt"nim compile --define:release --hints:off --outdir:{bindir}"
  cmdTail = fmt" --run {srcpath} --puzzle:{puzzle} --input:{input}"

echo fmt"day: {day.parseInt}"
discard execShellCmd(cmdHead & cmdTail)
