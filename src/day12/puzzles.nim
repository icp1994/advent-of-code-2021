import std/[parseopt, sets, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let ipathut = initOp.val

var graph: Table[string, HashSet[string]]
for l in lines(ipathut):
  let
    xy = l.split("-")
    x = xy[0]
    y = xy[1]
  graph.mgetOrPut(x, initHashSet[string]()).incl(y)
  graph.mgetOrPut(y, initHashSet[string]()).incl(x)

if puzzle == "one" or puzzle == "both":
  var nbPaths = 0
  proc traverse(node: string, path: HashSet[string]) =
    for node in graph[node]:
      if node == "end":
        nbPaths.inc
        continue
      var path = path
      if node[0].isLowerAscii:
        if path.containsOrIncl(node):
          continue
      traverse(node, path)

  traverse("start", ["start"].toHashSet)
  echo "puzzle one: ", nbPaths

if puzzle == "two" or puzzle == "both":
  var nbPaths = 0
  proc traverse(node: string, path: HashSet[string], twiced: bool) =
    for node in graph[node]:
      if node == "start":
        continue
      if node == "end":
        nbPaths.inc
        continue
      var
        path = path
        twiced = twiced
      if node[0].isLowerAscii:
        if path.containsOrIncl(node):
          if twiced:
            continue
          twiced = true
      traverse(node, path, twiced)

  traverse("start", ["start"].toHashSet, false)
  echo "puzzle two: ", nbPaths
