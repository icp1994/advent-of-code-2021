import std/[parseopt, strutils, sugar]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val
  
let commands = collect(newSeq):
  for line in lines(input):
    let cmd = line.split()
    (direction: cmd[0], value: cmd[1].parseInt)

if puzzle == "one" or puzzle == "both":
  var horizontal, depth = 0
  for cmd in commands:
    if cmd.direction == "forward":
      horizontal += cmd.value
    if cmd.direction == "down":
      depth += cmd.value
    if cmd.direction == "up":
      depth -= cmd.value

  echo "puzzle one: ", $(horizontal * depth)

if puzzle == "two" or puzzle == "both":
  var horizontal, depth, aim = 0
  for cmd in commands:
    if cmd.direction == "forward":
      horizontal += cmd.value
      depth += aim * cmd.value
    if cmd.direction == "down":
      aim += cmd.value
    if cmd.direction == "up":
      aim -= cmd.value

  echo "puzzle two: ", $(horizontal * depth)
