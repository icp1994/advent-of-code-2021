import std/[parseopt, strutils]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let energies = [1, 10, 100, 1000]
var
  ri, hpi = 0
  rps: array[4, int]
  hps: array[7, int]
  hallway: array[11, int]
  permL, permR: array[4, seq[(int, int)]]
  data = input.readFile.strip.splitLines

for i, c in data[2][1..^2]:
  hallway[i] = -1
  let mbAmphipod = c.ord - 'A'.ord
  if mbAmphipod in 0..3:
    rps[ri] = i
    ri.inc
  else:
    hps[hpi] = i
    hpi.inc

for ri in 0..3:
  var resL, resR: seq[(int, int)]
  for idx in countdown(rps[ri] - 1, 0, 1):
    if idx notin rps:
      resL.add((idx, (rps[ri] - idx)))
  permL[ri] = resL
  for idx in (rps[ri] + 1)..hallway.high:
    if idx notin rps:
      resR.add((idx, (idx - rps[ri])))
  permR[ri] = resR

if puzzle == "one" or puzzle == "both":
  type Config = tuple[
      hallway: array[11, int],
      rooms: array[4, array[2, int]],
      energy: int
    ]

  var rooms: array[4, array[2, int]]
  for i, line in data[2..^2]:
    for ri, rp in rps:
      rooms[ri][i] = line[1 + rp].ord - 'A'.ord
  let config: Config = (hallway, rooms, 0)
  
  func solvedPeek(config: Config, ri: int): (bool, int) =
    let room = config.rooms[ri]
    for i, mbAmphipod in room:
      if mbAmphipod != -1:
        for j in i..room.high:
          if room[j] != ri:
            return (false, i)
        return (true, i)
    return (true, room.len)

  func isSolved(config: Config): bool =
    for ri in 0..3:
      if config.solvedPeek(ri) != (true, 0):
        return false
    return true

  proc destined(config: Config, ri: int, start: int): (bool, int, int) =
    let (isSolved, top) = config.solvedPeek(ri)
    if isSolved:
      var (boundL, boundH) = (start, rps[ri])
      if start > rps[ri]:
        (boundL, boundH) = (rps[ri], start)
      for pos in (boundL + 1)..(boundH - 1):
        if config.hallway[pos] != -1:
          return (false, 0, 0)
      return (true, boundH - boundL + top, top - 1)

  proc move(config: Config): seq[Config] =
    for hp in hps:
      let ri = config.hallway[hp]
      if ri != -1:
        let (isDestined, steps, pos) = config.destined(ri, hp)
        if isDestined:
          var (h, r, e) = config
          r[ri][pos] = ri
          h[hp] = -1 
          e.inc(steps * energies[ri])
          result.add((h, r, e))

    for i, room in config.rooms:
      let (isSolved, top) = config.solvedPeek(i)
      if not isSolved:
        let ri = room[top]
        if ri != i:
          let (isDestined, steps, pos) = config.destined(ri, rps[i])
          if isDestined:
            var (h, r, e) = config
            r[ri][pos] = ri
            r[i][top] = -1
            e.inc((1 + top + steps) * energies[ri])
            result.add((h, r, e))
            continue
        for (idx, dist) in permL[i]:
          if config.hallway[idx] != -1:
            break
          var (h, r, e) = config
          h[idx] = ri
          r[i][top] = -1
          e.inc((1 + top + dist) * energies[ri])
          result.add((h, r, e))
        for (idx, dist) in permR[i]:
          if config.hallway[idx] != -1:
            break
          var (h, r, e) = config
          h[idx] = ri
          r[i][top] = -1
          e.inc((1 + top + dist) * energies[ri])
          result.add((h, r, e))

  var
    cache: seq[Config]
    cc = config.move
    res = high(int)

  while cc.len != 0:
    cache = @[]
    for cfg in cc:
      if cfg.energy < res:
        if cfg.isSolved:
          res = cfg.energy
        else:
          for nc in cfg.move:
            if nc.energy < res:
              cache.add(nc)
    cc = cache

  echo "puzzle one: ", res

if puzzle == "two" or puzzle == "both":
  data.insert("  #D#C#B#A#", 3)
  data.insert("  #D#B#A#C#", 4)

  type Config = tuple[
      hallway: array[11, int],
      rooms: array[4, array[4, int]],
      energy: int
    ]

  var rooms: array[4, array[4, int]]
  for i, line in data[2..^2]:
    for ri, rp in rps:
      rooms[ri][i] = line[1 + rp].ord - 'A'.ord
  let config: Config = (hallway, rooms, 0)

  func solvedPeek(config: Config, ri: int): (bool, int) =
    let room = config.rooms[ri]
    for i, mbAmphipod in room:
      if mbAmphipod != -1:
        for j in i..room.high:
          if room[j] != ri:
            return (false, i)
        return (true, i)
    return (true, room.len)

  func isSolved(config: Config): bool =
    for ri in 0..3:
      if config.solvedPeek(ri) != (true, 0):
        return false
    return true

  proc destined(config: Config, ri: int, start: int): (bool, int, int) =
    let (isSolved, top) = config.solvedPeek(ri)
    if isSolved:
      var (boundL, boundH) = (start, rps[ri])
      if start > rps[ri]:
        (boundL, boundH) = (rps[ri], start)
      for pos in (boundL + 1)..(boundH - 1):
        if config.hallway[pos] != -1:
          return (false, 0, 0)
      return (true, boundH - boundL + top, top - 1)

  proc move(config: Config): seq[Config] =
    for hp in hps:
      let ri = config.hallway[hp]
      if ri != -1:
        let (isDestined, steps, pos) = config.destined(ri, hp)
        if isDestined:
          var (h, r, e) = config
          r[ri][pos] = ri
          h[hp] = -1 
          e.inc(steps * energies[ri])
          result.add((h, r, e))

    for i, room in config.rooms:
      let (isSolved, top) = config.solvedPeek(i)
      if not isSolved:
        let ri = room[top]
        if ri != i:
          let (isDestined, steps, pos) = config.destined(ri, rps[i])
          if isDestined:
            var (h, r, e) = config
            r[ri][pos] = ri
            r[i][top] = -1
            e.inc((1 + top + steps) * energies[ri])
            result.add((h, r, e))
            continue
        for (idx, dist) in permL[i]:
          if config.hallway[idx] != -1:
            break
          var (h, r, e) = config
          h[idx] = ri
          r[i][top] = -1
          e.inc((1 + top + dist) * energies[ri])
          result.add((h, r, e))
        for (idx, dist) in permR[i]:
          if config.hallway[idx] != -1:
            break
          var (h, r, e) = config
          h[idx] = ri
          r[i][top] = -1
          e.inc((1 + top + dist) * energies[ri])
          result.add((h, r, e))

  var
    cache: seq[Config]
    cc = config.move
    res = high(int)

  while cc.len != 0:
    cache = @[]
    for cfg in cc:
      if cfg.energy < res:
        if cfg.isSolved:
          res = cfg.energy
        else:
          for nc in cfg.move:
            if nc.energy < res:
              cache.add(nc)
    cc = cache

  echo "puzzle two: ", res
