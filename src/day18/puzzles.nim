import std/[parseopt, sequtils, strutils]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

type
  Scale = tuple[value: int, level: int]
  Fish = seq[Scale]

func parseFish(fish: string): Fish =
  var level = 0
  for c in fish:
    case c
    of '[':
      level.inc
    of ']':
      level.dec
    of ',':
      continue
    else:
      result.add((($c).parseInt, level))

func explode(fish: Fish): Fish  =
  result = fish
  for idx, scale in fish[0..^2]:
    if (scale.level > 4) and (scale.level == fish[idx + 1].level):
      result[idx] = (0, scale.level - 1)
      if idx > 0:
        result[idx - 1].value.inc(scale.value)
      if idx < fish.high - 1:
        result[idx + 2].value.inc(fish[idx + 1].value)
      result.delete(idx + 1)
      break

func split(fish: Fish): Fish =
  result = fish
  for idx, scale in fish:
    if scale.value > 9:
      let vl = scale.value div 2
      let vr = scale.value - vl
      result[idx] = (vl, scale.level + 1)
      result.insert((vr, scale.level + 1), idx + 1)
      break

func reduce(fish: Fish): Fish =
  result = fish
  while true:
    let cache = result
    result = cache.explode
    if result.len == cache.len:
      result = cache.split
      if result.len == cache.len:
        break

func `+`(fl: Fish, fr: Fish): Fish =
  for scale in fl:
    result.add((scale.value, scale.level + 1))
  for scale in fr:
    result.add((scale.value, scale.level + 1))
  result = reduce(result)

proc magnitude(fish: Fish): int =
  var cache = fish
  while cache.len != 1:
    let fishT = cache
    for idx, scale in fishT:
      if scale.level == fishT[idx + 1].level:
        cache[idx] = (3 * scale.value + 2 * fishT[idx + 1].value, scale.level - 1)
        cache.delete(idx + 1)
        break
  
  assert cache[0].level == 0
  result = cache[0].value

let fishes = input.readFile.strip.splitLines.map(parseFish)
if puzzle == "one" or puzzle == "both":
  echo "puzzle one: ", fishes.foldl(a + b).magnitude

if puzzle == "two" or puzzle == "both":
  var res = 0
  for idx, fl in fishes[0..^2]:
    for fr in fishes[idx + 1 .. ^1]:
      res = res.max((fl + fr).magnitude).max((fr + fl).magnitude)
  echo "puzzle two: ", res
