import std/[parseopt, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let
  data = readFile(input).strip().split("\n\n")
  polymer = data[0]

var
  elemFreqs: CountTable[string]
  adjPairs: CountTable[string]
  rules: Table[string, (string, string, string)]

elemFreqs.inc($polymer[^1])
for idx, elem in polymer[0..^2]:
  elemFreqs.inc($elem)
  adjPairs.inc(elem & polymer[idx + 1])

for rule in data[1].splitLines:
  let
    line = rule.split(" -> ")
    elemPair = line[0]
    toInsert = line[1]
  rules[elemPair] = (toInsert, elemPair[0] & toInsert, toInsert & elemPair[1])

for step in 1..40:
  var newAdjPairs: CountTable[string]
  for elemPair, freq in adjPairs.pairs:
    let (toInsert, newPairLeft, newPairRight) = rules[elemPair]
    elemFreqs.inc(toInsert, freq)
    newAdjPairs.inc(newPairLeft, freq)
    newAdjPairs.inc(newPairRight, freq)
  adjPairs = newAdjPairs

  if step == 10:
    if puzzle != "two":
      echo "puzzle one: ", (elemFreqs.largest.val - elemFreqs.smallest.val)
      if puzzle == "one":
        break
  elif step == 40:
    echo "puzzle two: ", (elemFreqs.largest.val - elemFreqs.smallest.val)
