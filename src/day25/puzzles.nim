import std/[parseopt, sets, enumerate]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

var
  east, south, herd: HashSet[(int, int)]
  eastmax, southmax: int

for r, row in enumerate(input.lines):
  for c, sf in row:
    (eastmax, southmax) = (c, r)
    if sf == '>':
      east.incl((r, c))
      herd.incl((r, c))
    elif sf == 'v':
      south.incl((r, c))
      herd.incl((r, c))

if puzzle == "one" or puzzle == "both":
  var
    movefrom, movto: HashSet[(int, int)]
    nbMovers, steps: int

  while true:
    steps.inc
    nbMovers = 0
    movefrom.clear
    movto.clear
    
    for sc in east:
      let dest = (sc[0], (sc[1] + 1) mod eastmax.succ)
      if dest notin herd:
        movefrom.incl(sc)
        movto.incl(dest)
    
    nbMovers.inc(movefrom.len)
    if nbMovers > 0:
      east.excl(movefrom)
      herd.excl(movefrom)
      east.incl(movto)
      herd.incl(movto)

    movefrom.clear
    movto.clear
    
    for sc in south:
      let dest = ((sc[0] + 1) mod southmax.succ, sc[1])
      if dest notin herd:
        movefrom.incl(sc)
        movto.incl(dest)
    
    nbMovers.inc(movefrom.len)
    if nbMovers == 0:
      echo "puzzle one: ", steps
      break

    south.excl(movefrom)
    herd.excl(movefrom)
    south.incl(movto)
    herd.incl(movto)

if puzzle == "two" or puzzle == "both":
  echo "puzzle two: ", "merry christmas!!!"
