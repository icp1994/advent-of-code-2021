import std/[math, parseopt, parseutils, strutils, sugar, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let nums =  readFile(input).strip().splitLines()

func winnerInPos(nums: seq[string], pos: int, mcv: bool): char =
  var counter: CountTable[char]
  for num in nums:
    counter.inc(num[pos])

  if mcv:
    if counter['0'] > counter['1']:
      result = '0'
    else:
      result = '1'
  else:
    if counter['1'] < counter['0']:
      result = '1'
    else:
      result = '0'

if puzzle == "one" or puzzle == "both":
  let numsize = len(nums[0])
  let gammaBits = collect(newSeq):
    for pos in 0 ..< numsize:
      winnerInPos(nums, pos, true)

  var gammaRate: int
  discard parseBin(join(gammaBits), gammaRate)
  let epsilonRate = 2 ^ numsize - 1 - gammaRate
  echo "puzzle one: ", gammaRate * epsilonRate


if puzzle == "two" or puzzle == "both":
  var ratings: seq[string]
  for mcv in [true, false]:
    var pos = 0
    var numsLeft = nums
    while len(numsLeft) > 1:
      var indices: Table[char, seq[int]]
      for idx, num in numsLeft:
        indices.mgetOrPut(num[pos], @[]).add(idx)
      numsLeft = collect(newSeq):
        for idx in indices[winnerInPos(numsLeft, pos, mcv)]:
          numsLeft[idx]
      inc pos
    ratings.add(numsLeft[0])

  var o2GenRate, co2ScrubRate: int
  discard parseBin(ratings[0], o2GenRate)
  discard parseBin(ratings[1], co2ScrubRate)
  echo "puzzle two: ", o2GenRate * co2ScrubRate
