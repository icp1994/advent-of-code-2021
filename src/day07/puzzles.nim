import std/[parseopt, sequtils, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let nums = readFile(input).strip().split(",").map(parseInt)

if puzzle == "one" or puzzle == "both":
  var posFuel: CountTable[int]
  for pos in min(nums) .. max(nums):
    var fuel = 0
    for num in nums:
      fuel += abs(pos - num)
    posFuel[pos] = fuel
  echo "puzzle one: ", posFuel.smallest.val

if puzzle == "two" or puzzle == "both":
  var posFuel: CountTable[int]
  for pos in min(nums) .. max(nums):
    var fuel = 0
    for num in nums:
      let d = abs(pos - num)
      fuel += (d * (d + 1)) div 2
    posFuel[pos] = fuel
  echo "puzzle two: ", posFuel.smallest.val
