import std/[heapqueue, parseopt, sequtils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

func seScore(line: string, match: Table[char, char], score: Table[char, int]): int =
  var stack: seq[char]
  for c in line:
    if c in match:
      stack.add(c)
    elif c != match[stack.pop]:
      return score[c]

func cmpScore(line: string, match: Table[char, char], score: Table[char, int]): int =
  var stack: seq[int]
  for c in line:
    if c in match:
      stack.add(score[match[c]])
    else:
      discard stack.pop
  return foldr(stack, 5 * b + a)

let match = {'(': ')', '[': ']', '{': '}', '<': '>'}.toTable
var incompletes: seq[string]
if puzzle == "one" or puzzle == "both":
  let score = {')': 3, ']': 57, '}': 1197, '>': 25137}.toTable
  var res = 0
  for line in lines(input):
    let s = seScore(line, match, score)
    if s == 0:
      incompletes.add(line)
    else:
      res.inc(s)
  echo "puzzle one: ", res

if puzzle == "two" or puzzle == "both":
  var res: HeapQueue[int]
  let score = {')': 1, ']': 2, '}': 3, '>': 4}.toTable
  for line in incompletes[0 .. ((incompletes.len - 1) div 2)]:
    res.push(line.cmpScore(match, score))
  for line in incompletes[((incompletes.len + 1) div 2) .. ^1]:
    discard res.pushpop(cmpScore(line, match, score))
  echo "puzzle two: ", res.pop
