import std/[enumerate, parseopt, sets, strutils, sugar]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

type
  Pixel = tuple[r: int, c: int]
  Image = HashSet[Pixel]

let
  data = input.readFile.split("\n\n")
  algo: HashSet[int] = collect:
    for i, c in data[0]:
      if c == '#':
        {i}

var
  rmin, cmin = 0
  rmax, cmax: int
  image: Image

for r, row in enumerate(data[1].splitLines):
  for c, ident in row:
    (rmax, cmax) = (r, c)
    if ident == '#':
      image.incl((r, c))

func cover(pixel: Pixel): array[9, Pixel] =
  var k = 0
  for i in -1..1:
    for j in -1..1:
      result[k] = (pixel.r + i, pixel.c + j)
      k.inc

proc enhanceInBounds(image: var Image, northwest: Pixel, southeast: Pixel) =
  var enhanced: Image
  for r in northwest.r..southeast.r:
    for c in northwest.c..southeast.c:
      let pixel = (r, c)
      var idx = 0
      for p in pixel.cover:
        idx *= 2
        if p in image:
          idx.inc
      if idx in algo:
        enhanced.incl(pixel)
  image = enhanced

for t in 1..25:
  (rmin, cmin) = (rmin - 2, cmin - 2)
  (rmax, cmax) = (rmax + 2, cmax + 2)
  enhanceInBounds(image, (rmin - 1, cmin - 1), (rmax + 1, cmax + 1))
  enhanceInBounds(image, (rmin, cmin), (rmax, cmax))
  if t == 1 and puzzle != "two":
    echo "puzzle one: ", image.len
    if puzzle == "one":
      break

if puzzle != "one":
  echo "puzzle two: ", image.len
