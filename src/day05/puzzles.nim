import std/[enumerate, parseopt, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

var ventsParallel, ventsDiagonal: CountTable[(int, int)]
for line in readFile(input).strip().splitLines():
  let
    xy12 = line.split(" -> ")
    xy1 = xy12[0].split(",")
    xy2 = xy12[1].split(",")
    x1 = xy1[0].parseInt
    y1 = xy1[1].parseInt
    x2 = xy2[0].parseInt
    y2 = xy2[1].parseInt

  if x1 == x2:
    for ty in min(y1, y2) .. max(y1, y2):
      ventsParallel.inc((x1, ty))
  elif y1 == y2:
    for tx in min(x1, x2) .. max(x1, x2):
      ventsParallel.inc((tx, y1))
  else:
    let slopeIsPositive = (x2 - x1) * (y2 - y1) > 0
    for shift, tx in enumerate(min(x1, x2) .. max(x1, x2)):
      if slopeIsPositive:
        ventsDiagonal.inc((tx, (min(y1, y2) + shift)))
      else:
        ventsDiagonal.inc((tx, (max(y1, y2) - shift)))

if puzzle == "one" or puzzle == "both":
  var res = 0
  for covers in ventsParallel.values:
    if covers > 1:
      inc res
  echo "puzzle one: ", res

if puzzle == "two" or puzzle == "both":
  ventsDiagonal.merge(ventsParallel)
  var res = 0
  for covers in ventsDiagonal.values:
    if covers > 1:
      inc res
  echo "puzzle two: ", res
