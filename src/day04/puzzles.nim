import std/[enumerate, intsets, math, parseopt, sequtils, strutils, sugar, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let
  data = readFile(input).strip().split("\n\n")
  nums = data[0].split(",").mapIt(it.parseInt)
  grids: seq[Table[int, (int, int)]] = collect:
    for board in data[1 .. ^1]:
      var grid: Table[int, (int, int)]
      for row, line in enumerate(board.splitLines()):
        for col, num in enumerate(line.splitWhitespace()):
          grid[num.parseInt] = (row, col)
      grid
  boardSize = grids[0].len.float.sqrt.int

func bingoCheck(boardState: seq[seq[bool]]): bool =
  let boardSize = boardState.len
  assert boardState[0].len == boardSize
  var boardTrans = newSeqWith(boardSize, newSeq[bool](boardSize))
  for row in 0 ..< boardSize:
    if boardState[row].allIt(it):
      return true
    for col in 0 ..< boardSize:
      boardTrans[row][col] = boardState[col][row]

  for col in 0 ..< boardSize:
    if boardTrans[col].allIt(it):
      return true

  return false

var
  boardStates = newSeqWith(
    grids.len, newSeqWith(boardSize, newSeq[bool](boardSize))
  )
  finished: IntSet

block run:
  for num in nums:
    for idx, grid in grids:
      if idx in finished:
        continue
      if num in grid:
        boardStates[idx][grid[num][0]][grid[num][1]] = true
        if bingoCheck(boardStates[idx]):
          finished.incl(idx)
          if finished.len in @[1, grids.len]:
            var res = 0
            for n, (r, c) in grid.pairs:
              if boardStates[idx][r][c] == false:
                res += n
            if finished.len == 1:
              if puzzle != "two":
                echo "puzzle one: ", res * num
              if puzzle == "one":
                break run
            if finished.len == grids.len:
              echo "puzzle two: ", res * num
              break run
