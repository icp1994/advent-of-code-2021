import std/[parseopt, sequtils, sets, strutils, sugar, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

type
  Beacon = tuple[x: int, y: int, z: int]
  Report = HashSet[Beacon]

proc `+`(bi: Beacon, bj: Beacon): Beacon =
  return (bi.x + bj.x, bi.y + bj.y, bi.z + bj.z)

proc `-`(bi: Beacon, bj: Beacon): Beacon =
  return (bi.x - bj.x, bi.y - bj.y, bi.z - bj.z)

func axesOris(beacon: Beacon): array[24, Beacon] =
  let (x, y, z) = beacon
  result = [
    (x, y, z), (x, -y, -z), (x, z, -y), (x, -z, y),
    (-x, y, -z), (-x, -y, z), (-x, z, y), (-x, -z, -y),
    (y, x, -z), (y, -x, z), (y, z, x), (y, -z, -x),
    (-y, x, z), (-y, -x, -z), (-y, z, -x), (-y, -z, x),
    (z, x, y), (z, -x, -y), (z, y, -x), (z, -y, x),
    (-z, x, -y), (-z, -x, y), (-z, y, x), (-z, -y, -x)
  ]

var reports: seq[Report] = collect:
  for data in input.readFile.strip.split("\n\n"):
    let report: Report = collect:
      for beacon in data.splitLines[1..^1]:
        let coord = beacon.split(",").map(parseInt)
        {(coord[0], coord[1], coord[2])}
    report

var
  settled: HashSet[int] = [0].toHashSet
  scanners: Table[int, Beacon] = {0: (0, 0, 0)}.toTable
  beacons: HashSet[Beacon] = reports[0]
  unsettled: HashSet[int] = (1..reports.high).toSeq.toHashSet

let oris: HashSet[int] = (0..<24).toSeq.toHashSet
block settle:
  while true:
    let reportI = reports[settled.pop]
    var toExcl: HashSet[int]
    for idx in unsettled:
      let reportJ = reports[idx]
      block pair:
        var remaining = reportI.len
        for beaconI in reportI:
          remaining.dec
          if remaining == 10:
            break
          for beaconJ in reportJ:
            let beaconOris = beaconJ.axesOris
            for ori in oris:
              var overlaps = 0
              let
                scannerJ = beaconI - beaconOris[ori]
                absolutes: Report = collect:
                  for beacon in reportJ:
                    let absBeacon = scannerJ + beacon.axesOris[ori]
                    if absBeacon in reportI:
                      overlaps.inc
                    {absBeacon}
              if overlaps >= 12:
                reports[idx] = absolutes
                settled.incl(idx)
                scanners[idx] = scannerJ
                beacons.incl(absolutes)
                toExcl.incl(idx)
                if unsettled.len == toExcl.len:
                  break settle
                break pair
    unsettled.excl(toExcl)

func manhattan(bl: Beacon, br: Beacon): int =
  result.inc((bl.x - br.x).abs)
  result.inc((bl.y - br.y).abs)
  result.inc((bl.z - br.z).abs)

if puzzle == "one" or puzzle == "both":
  echo "puzzle one: ", beacons.len

if puzzle == "two" or puzzle == "both":
  var res = 0
  for i in 0..(scanners.len - 2):
    for j  in (i + 1)..(scanners.len - 1):
      res = res.max(manhattan(scanners[i], scanners[j]))
  echo "puzzle two: ", res
