import std/[parseopt, sequtils, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

#[
w[1]                          w[2]
z=26*z[0]+w[1]+4              z=26*z[1]+w[2]+11

w[3]                          w[4]=(w[3]+7-14)=(w[3]-7)
z=26*z[2]+w[3]+7              z=26*z[1]+w[2]+11

w[5]                          w[6]=(w[5]+11-10)=(w[5]+1)
z=26*z[4]+w[5]+11             z=26*z[1]+w[2]+11

w[7]                          w[8]
z=26*z[6]+w[7]+9              z=26*z[7]+w[8]+12

w[9]=(w[8]+12-7)=(w[8]+5)     w[10]
z=26*z[6]+w[7]+9              z=26*z[9]+w[10]+2

w[11]=(w[10]+2-2)=w[10]       w[12]=(w[7]+9-1)=(w[7]+8)
z=26*z[6]+w[7]+9              z=26*z[1]+w[2]+11

w[13]=(w[2]+11-4)=(w[2]+7)    w[14]=(w[1]+4-12)=(w[1]-8)
z=26*z[0]+w[1]+4              z=z[0]=0
]#

if puzzle == "one" or puzzle == "both":
  echo "puzzle one: ", 92928914999991

if puzzle == "two" or puzzle == "both":
  echo "puzzle two: ", 91811211611981

let
  monad = input.readFile.strip.splitLines.mapIt(it.split)
  indices = {"w": 0, "x": 1, "y": 2, "z": 3}.toTable

var mem = [0, 0, 0, 0]
proc alu(digits: string, monad: seq[seq[string]]): bool =
  var head, val: int
  for idx, ins in monad:
    if ins[0] == "inp":
      mem[0] = ($digits[head]).parseInt
      mem[1] = 0
      mem[2] = 0
      head.inc
    else:
      let idx = indices[ins[1]]
      try:
        val = ins[2].parseInt
      except ValueError:
        val = mem[indices[ins[2]]]
      case ins[0]
      of "add":
        mem[idx] = mem[idx] + val
      of "mul":
        mem[idx] = mem[idx] * val
      of "div":
        mem[idx] = mem[idx] div val
      of "mod":
        mem[idx] = mem[idx] mod val
      of "eql":
        mem[idx] = ord(mem[idx] == val)
  return (mem[^1] == 0)

#[
if puzzle == "one" or puzzle == "both":
  for num in countdown(99_999_999_999_999, 11_111_111_111_111, 1):
    let digits = $num
    if '0' notin digits:
      if digits.alu(monad):
        echo "puzzle one: ", num
        break

if puzzle == "two" or puzzle == "both":
  for num in 11_111_111_111_111..99_999_999_999_999:
    let digits = $num
    if '0' notin digits:
      if digits.alu(monad):
        echo "puzzle two: ", num
        break
]#
