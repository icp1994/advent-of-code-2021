import std/[parseopt, strutils, enumerate, tables, heapqueue]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

type
  Position = (int, int)
  Risks = Table[Position, int]

var
  risks: Risks
  dim = 0

for r, line in enumerate(lines(input)):
  for c, v in line:
    risks[(r, c)] = ($v).parseInt
  dim.inc

func nbrs(p: (int, int)): array[4, (int, int)] =
  let (r, c) = p
  return [(r + 1, c), (r, c + 1), (r - 1, c), (r, c - 1)]

proc traverse(risks: Risks, start: Position, fin: Position): int =
  let maxRisk = 10 * risks.len
  var
    queue: HeapQueue[(int, Position)]
    ltr: Risks
  for p in risks.keys:
    queue.push((maxRisk, p))
  ltr[start] = 0
  queue.push((0, start))

  while queue.len != 0:
    let (d, p) = queue.pop
    if d > ltr.getOrDefault(p, maxRisk):
      continue
    for q in p.nbrs:
      let nd = d + risks.getOrDefault(q, maxRisk)
      if nd < ltr.getOrDefault(q, maxRisk):
        ltr[q] = nd
        queue.push((nd, q))
  return ltr[fin]

let start = (0, 0)
if puzzle == "one" or puzzle == "both":
  let fin = (dim - 1, dim - 1)
  echo "puzzle one: ", traverse(risks, start, fin)

if puzzle == "two" or puzzle == "both":
  var lrisks = risks
  for pos, rl in risks.pairs:
    let (r, c) = pos
    for i in 0..4:
      for j in 0..4:
        let t = (rl + i + j) mod 9
        lrisks[(r + i * dim, c + j * dim)] = if t == 0: 9 else: t
  let fin = (5 * dim - 1, 5 * dim - 1)
  echo "puzzle two: ", traverse(lrisks, start, fin)
