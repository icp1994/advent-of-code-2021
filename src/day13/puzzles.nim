import std/[parseopt, sequtils, strutils, enumerate, sets]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let data = readFile(input).strip().split("\n\n")
var points: HashSet[(int, int)]
var mx, my = 0

for line in data[0].splitLines:
  let xy = line.split(",").map(parseInt)
  points.incl((xy[0], xy[1]))
  mx = max(mx, xy[0])
  my = max(my, xy[1])

for idx, line in enumerate(data[1].splitLines):
  var folded: HashSet[(int, int)]
  if line.split(" ")[^1].split("=")[0] == "x":
    for (x, y) in points:
      folded.incl((min(x, mx - x), y))
    mx = mx div 2 - 1
  else:
    for (x, y) in points:
      folded.incl((x, min(y, my - y)))
    my = my div 2 - 1
  points = folded
  if idx == 0:
    if puzzle != "two":
      echo "puzzle one: ", points.len
      if puzzle == "one":
        break

if puzzle == "two" or puzzle == "both":
  echo "puzzle two: "
  let align = " ".repeat("puzzle two: ".len)
  for y in 0..my:
    var line = align
    for x in 0..mx:
      let c = if (x, y) in points: '#' else: '.'
      line.add(c)
    echo line
