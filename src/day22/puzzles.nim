import std/[parseopt, re, sequtils, strutils]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

type
  Bound = tuple[l: int, h: int]
  Cuboid = tuple[x: Bound, y: Bound, z: Bound]

func size(cuboid: Cuboid): int =
  result = 1
  result *= (cuboid.x.h - cuboid.x.l + 1)
  result *= (cuboid.y.h - cuboid.y.l + 1)
  result *= (cuboid.z.h - cuboid.z.l + 1)

func detach(ci: Cuboid, cj: Cuboid): seq[Cuboid] =
  var
    (xbl, xbh) = ci.x
    (ybl, ybh) = ci.y

  if (cj.x.l > ci.x.h) or (cj.x.h < ci.x.l) or
    (cj.y.l > ci.y.h) or (cj.y.h < ci.y.l) or
    (cj.z.l > ci.z.h) or (cj.z.h < ci.z.l):
    return @[ci]

  if cj.x.l > ci.x.l:
    xbl = cj.x.l
    result.add(((ci.x.l, xbl - 1), (ci.y.l, ci.y.h), (ci.z.l, ci.z.h)))

  if cj.x.h < ci.x.h:
    xbh = cj.x.h
    result.add(((xbh + 1, ci.x.h), (ci.y.l, ci.y.h), (ci.z.l, ci.z.h)))

  if cj.y.l > ci.y.l:
    ybl = cj.y.l
    result.add(((xbl, xbh), (ci.y.l, ybl - 1), (ci.z.l, ci.z.h)))

  if cj.y.h < ci.y.h:
    ybh = cj.y.h
    result.add(((xbl, xbh), (ybh + 1, ci.y.h), (ci.z.l, ci.z.h)))

  if cj.z.l > ci.z.l:
    result.add(((xbl, xbh), (ybl, ybh), (ci.z.l, cj.z.l - 1)))

  if cj.z.h < ci.z.h:
    result.add(((xbl, xbh), (ybl, ybh), (cj.z.h + 1, ci.z.h)))

func detach(cc: seq[Cuboid], cj: Cuboid): seq[Cuboid] =
  for ci in cc:
    result.add(ci.detach(cj))

func attach(cc: seq[Cuboid], cj: Cuboid): seq[Cuboid] =
  for ci in cc:
    result.add(ci.detach(cj))
  result.add(cj)

var cc: seq[Cuboid]
for line in input.lines:
  if line =~ re"^(on|off) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)$":
    let
      op = matches[0]
      bounds = matches[1..6].map(parseInt)
      xb: Bound = (bounds[0], bounds[1])
      yb: Bound = (bounds[2], bounds[3])
      zb: Bound = (bounds[4], bounds[5])
      cuboid: Cuboid = (xb, yb, zb)

    if op == "on":
      cc = cc.attach(cuboid)
    else:
      assert op == "off"
      cc = cc.detach(cuboid)
  else:
    assert false

let res = cc.foldl(a + b.size, 0)
if puzzle == "one" or puzzle == "both":
  cc = cc.detach(((-50, 50), (-50, 50), (-50, 50)))
  echo "puzzle one: ", (res - cc.foldl(a + b.size, 0))

if puzzle == "two" or puzzle == "both":
  echo "puzzle two: ", res
