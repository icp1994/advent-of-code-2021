import std/[parseopt, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let
  data = readFile(input).strip().splitLines()
  posA = data[0].split(": ")[1].parseInt
  posB = data[1].split(": ")[1].parseInt

if puzzle == "one" or puzzle == "both":
  var
    scoreA, scoreB, rolls = 0
    sum = -4
    turnA = true
  
  const target = 1000
  proc play(posA: int, posB: int): int =
    sum.inc(9)
    if turnA:
      if scoreB >= target:
        return scoreA * rolls
      else:
        let posA = (posA + sum) mod 10 + 1
        scoreA.inc(posA)
        rolls.inc(3)
        turnA = false
        return play(posA, posB)
    else:
      if scoreA >= target:
        return scoreB * rolls
      else:
        let posB = (posB + sum) mod 10 + 1
        scoreB.inc(posB)
        rolls.inc(3)
        turnA = true
        return play(posA, posB)

  echo "puzzle one: ", play(posA, posB)

if puzzle == "two" or puzzle == "both":
  var
    winsA, winsB = 0
    turnA = true
    multipliers: CountTable[int] 
  
  for i in 1..3:
    for j in 1..3:
      for k in 1..3:
        multipliers.inc(i + j + k - 1)
  
  const target = 21
  proc play(posA: int, posB: int, scoreA:int, scoreB: int, multi: int) =
    if turnA:
      if scoreB >= target:
        winsB.inc(multi)
      else:
        for k, v in multipliers.pairs:
          let
            posA = (posA + k) mod 10 + 1
            scoreA = scoreA + posA
            multi = multi * v
          turnA = false
          play(posA, posB, scoreA, scoreB, multi)
    else:
      if scoreA >= target:
        winsA.inc(multi)
      else:
        for k, v in multipliers.pairs:
          let
            posB = (posB + k) mod 10 + 1
            scoreB = scoreB + posB
            multi = multi * v
          turnA = true
          play(posA, posB, scoreA, scoreB, multi)
  
  play(posA, posB, 0, 0, 1)
  echo "puzzle two: ", winsA.max(winsB)
