import std/[parseopt, sequtils, strutils, tables]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

var memcache: Table[int, int]
proc familySize(birthday: int, upto: int): int =
  if birthday notin memcache:
    if birthday + 9 > upto:
      memcache[birthday] = 1
    else:
      var total = 1
      for offspring in countup(birthday + 9, upto, 7):
        total += familySize(offspring, upto)
      memcache[birthday] = total
  return memcache[birthday]

let nums = readFile(input).strip().split(",").map(parseInt)
if puzzle == "one" or puzzle == "both":
  var res = 0
  for num in nums:
    res += familySize(num - 8, 80)
  echo "puzzle one: ", res

if puzzle == "two" or puzzle == "both":
  memcache.clear
  var res = 0
  for num in nums:
    res += familySize(num - 8, 256)
  echo "puzzle two: ", res
