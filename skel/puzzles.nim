import std/[parseopt, sequtils, strutils, sugar]

var initOP = initOptParser()
initOp.next()
let puzzle = initOp.val
initOp.next()
let input = initOp.val

let
  nums = readFile(input).strip().splitLines().map(parseInt)

if puzzle == "one" or puzzle == "both":
  echo "puzzle one: "

if puzzle == "two" or puzzle == "both":
  echo "puzzle two: "
