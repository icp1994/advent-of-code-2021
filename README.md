Compile the convenience `runner` binary with
```
nim compile --define:release --outdir:bin/ src/runner.nim
```

Then to run the puzzle `y` (`one`, `two`, or `both`) from day `x` (`1`, `23` etc)
```
./bin/runner [(--day|-d):{x}] [(--puzzle|-p):{y}] [input]
```

Alternatively, you can compile and run the specific source file of day `d` with
```
nim c -d:release --outdir:bin/ -r src/day{d}/puzzles.nim --puzzle:both --input:src/day{d}/input
```
Note, here `d` must be of length two with `0` padding when necessary.
